<?php
    session_start();

    include 'connect.php';
    include 'fonction_php_mistake.php';
    $menu_modif = 2;

    $oldname = $_POST["ancien_planete"];
    $newname = htmlspecialchars($_POST["nouveau_planete"]);

    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //On insère les données reçues
        $insertgenre = $dbco->prepare("UPDATE planet SET name = ? WHERE name = ?");
        
        $insertgenre->execute(array($newname, $oldname));
        $insertgenre->closeCursor();

        succes($menu_modif);

    }
    catch(PDOException $e){ 
        if ($e->getCode() == 23000) {

            erreur($e->getCode(), $menu_modif);
           
        } else {
            header("Location: administrateur.php");
        }
    }
    ?>