<?php
  session_start();

  include 'header.inc.php';
  include 'fonction_php_mistake.php';
  include 'navbar.inc.php';

  insntconnected_admin();
?>

<div class="row justify-content-center mt-4">
    <div class="card border-dark mb-3" style="max-width: 30rem;">
        <div class="card-header">
            <h1>Page Administrateur</h1>
        </div>
    </div>
</div>

<div class="row justify-content-center mt-4">
    <div class="card border-dark" style="max-width: 30rem;">
        <div class="card-header text-center">
            <h1>Menu d'action</h1>
        </div>
        <div class="card-body text-dark">

            <div class="row justify-content-center">
                <div class="col">
                    <a href="menu_creation.php" class="btn btn-dark active mt-4 mb-4" role="button"
                        aria-pressed="true">Menu
                        création</a>
                </div>
                <div class="col">
                    <a href="menu_modification.php" class="btn btn-dark active mt-4 mb-4" role="button"
                        aria-pressed="true">Menu
                        modification</a>
                </div>

                <div class="col">
                    <a href="menu_delete.php" class="btn btn-dark active mt-4 mb-4" role="button"
                        aria-pressed="true">Menu
                        suppression</a>
                </div>
            </div>



        </div>
    </div>
</div>

<?php
  include 'footer.inc.php';
?>