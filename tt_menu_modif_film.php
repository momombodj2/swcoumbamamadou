<?php
    session_start();

    include 'connect.php';
    include 'fonction_php_mistake.php';
    $menu_modif = 2;

    $new_nom_film = htmlspecialchars($_POST["new_nom_film"]);
    $old_nom_film = htmlspecialchars($_POST["old_nom_film"]);
    $opening = htmlspecialchars($_POST["opening"]);

    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //On insère les données reçues
        $insertfilm = $dbco->prepare("UPDATE film SET title = ? WHERE title = ?");
        $insertfilm->execute(array($new_nom_film, $old_nom_film));
        $insertfilm->closeCursor();

        $insertfilm = $dbco->prepare("UPDATE film SET opening = ? WHERE title = ?");
        $insertfilm->execute(array($opening, $old_nom_film));

        $insertfilm->closeCursor();

        succes($menu_modif);

    }
    catch(PDOException $e){ 
        if ($e->getCode() == 23000) {

            erreur($e->getCode(), $menu_modif);
           
        } else {
            header("Location: administrateur.php");
        }
    }
    ?>