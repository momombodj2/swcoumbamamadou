<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';


try {
    // On se connecte à MySQL
    $bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
} catch (Exception $e) {
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : ' . $e->getMessage());
}


?>



    <div class="row justify-content-center mt-2 mb-3">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h3>BIENVENUE SUR STARWARS SITE</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col">


                <div class="card">
                    <div class="card-header">
                        Quelques films
                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">
                            <p>
                                <img src="image/filmac1.jpg" alt="..." class="img-thumbnail">
                                <p class="text-danger">13/10/1999
                                    <B class="text-danger"> StarWars 1 : </B> </p>
                                <p size="+1">Avant de devenir un célèbre chevalier Jedi, et bien avant de se révéler l'âme
                                 la plus noire de la galaxie, Anakin Skywalker est un jeune esclave sur la planète Tatooine. 
                                 La Force est déjà puissante en lui et il est un remarquable pilote de Podracer. Le maître Jedi 
                                 Qui-Gon Jinn le découvre et entrevoit alors son immense potentiel. Pendant ce temps, l'armée de droïdes 
                                 de l'insatiable Fédération du Commerce a envahi Naboo dans le cadre d'un plan secret des Sith visant à 
                                 accroître leur pouvoir.</p>
                            </p>

                        </blockquote>
                    </div>


                    <div class="card">
                        
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p>
                                    <img src="image/filmac2.jpg" alt="..." class="img-thumbnail">
                                    <p class="text-danger">20/08/1980 </p>
                                    <B class="text-danger"> StarWars 5 : </B>
                                    <p size="+1">
                                    Malgré la destruction de l'Etoile Noire, l'Empire maintient son emprise sur la galaxie, 
                                    et poursuit sans relâche sa lutte contre l'Alliance rebelle. Basés sur la planète glacée de Hoth, 
                                    les rebelles essuient un assaut des troupes impériales. Parvenus à s'échapper, la princesse Leia, Han Solo, 
                                    Chewbacca et C-3P0 se dirigent vers Bespin, la cité des nuages gouvernée par Lando Calrissian, ancien compagnon 
                                    de Han.</p>
                                </p>
                            </blockquote>
                        </div>

                        <div class="card">
                           
                            <div class="card-body">
                                <blockquote class="blockquote mb-0">
                                    <p>
                                        <img src="image/filmac3.jpg" alt="..." class="img-thumbnail">
                                        <p class="text-danger">13/12/2017 </p>
                                        <B class="text-danger"> StarWars 8 :</B>
                                        <p size="+1">
                                        Le Premier Ordre étend ses tentacules aux confins de l'univers, 
                                        poussant la Résistance dans ses retranchements. Il est impossible de se sauver à la vitesse de 
                                        la lumière avec cet ennemi continuellement aux trousses. Cela n'empêche pas Finn et ses camarades de 
                                        tenter d'identifier une brèche chez leur adversaire. Pendant ce temps, Rey se trouve toujours sur la planète 
                                        Ahch-To pour convaincre Luke Skywalker de lui enseigner les rudiments de la Force.</p>
                                        
                                        
                                    </p>

                                </blockquote>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php // On récupère un contenu aléatoire dans la BDD
            $reponse = $bdd->prepare('SELECT DISTINCT * FROM people ');
            $reponse->execute();
            // On affiche chaque entrée une à une

            ?>
            <div class="col">
                <div class="row row-cols-1 row-cols-md-1">

                    <?php
                    for ($i = 0; $i <= 4; $i++) {
                        $donnees = $reponse->fetch();
                        affichage_personnage_visiteur($donnees['name'], $donnees['height'], $donnees['mass']);
                    ?>
                  
                    
                    <?php } ?>



                </div>
            </div>
        </div>

    </div>





    <?php
    include 'footer.inc.php';
    ?>