<?php
session_start();
include 'connect.php';


$dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);

if (isset($_POST['button_connexion'])) {
   $pseudoconnect = htmlspecialchars($_POST['pseudo']);
   $passwordconnect = ($_POST['password']);
   
   if (!empty($pseudoconnect) and !empty($passwordconnect)) {
      $requser = $dbco->prepare("SELECT * FROM membres WHERE pseudo = ? AND password = ?");
      $requser->execute(array($pseudoconnect, $passwordconnect));
      $userexist = $requser->rowCount();

      if ($userexist == 1) {
         $userinfo = $requser->fetch();
         $_SESSION['id'] = $userinfo['id'];
         $_SESSION['login'] = $userinfo['pseudo'];
         $_SESSION['password'] = $userinfo['password'];
         $_SESSION['role_id'] = $userinfo['role_id'];
         $_SESSION['isvoted'] = 0;

         if ($userinfo['role_id'] == 1) {
           
            header("Location: administrateur.php?id=" . $_SESSION['id']);
            
         } else {
            
            header("Location: mon_compte.php?id=" . $_SESSION['id']);
           
         }
      } else {
      
      header("Location: connexion_mistake.php");
         
      }
   } else {
      
      header("Location: connexion.php");
   }
}
