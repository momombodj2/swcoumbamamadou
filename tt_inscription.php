<?php
    session_start();

    include 'connect.php';
    
    $prenom = $_POST["prenom"];
    $email = $_POST["email"];
    $nom = $_POST["nom"];
    $password = $_POST["password"];
   
    $pseudo = $_POST["pseudo"];
    
    $role_id = 2;
    $id_f1_film = 0;
    $id_f2_film = 0;
    $id_f3_film = 0;




    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //On insère les données reçues
        $sth = $dbco->prepare("
            INSERT INTO membres(pseudo, nom, prenom, email, password, role_id, id_f1_film, id_f2_film, id_f3_film)
            VALUES(:pseudo, :nom, :prenom, :email, :password, :role_id, :id_f1_film, :id_f2_film, :id_f3_film)");


        $sth->bindParam(':nom',$nom);
        $sth->bindParam(':prenom',$prenom);
        $sth->bindParam(':email',$email);
        $sth->bindParam(':password',$password);
        
        $sth->bindParam(':pseudo',$pseudo);
        $sth->bindParam(':role_id',$role_id);
        $sth->bindParam(':id_f1_film',$id_f1_film);
        $sth->bindParam(':id_f2_film',$id_f2_film);
        $sth->bindParam(':id_f3_film',$id_f3_film);

        $sth->execute();  
        
        $requser = $dbco->prepare("SELECT * FROM membres WHERE pseudo = ?");
        $requser->execute(array($pseudo));
        $userexist = $requser->rowCount();

        if($userexist == 1) {
           $userinfo = $requser->fetch();
           $_SESSION['id'] = $userinfo['id']; // Table user
           $_SESSION['login'] = $userinfo['pseudo']; // table user 
           $_SESSION['password'] = $userinfo['password']; // table user
           $_SESSION['role_id'] = $userinfo['role_id'];
           $_SESSION['id_f1_film'] = $userinfo['id_f1_film'];
           $_SESSION['id_f2_film'] = $userinfo['id_f2_film'];
           $_SESSION['id_f3_film'] = $userinfo['id_f3_film'];
           
           if($userinfo['role_id'] == 1){//On renvoie l'utilisateur vers la page admin ( cas impossible )
           header("Location: administrateur.php?id=".$_SESSION['id']);
           }

           else{//On renvoie l'utilisateur vers la page mon compte correspondant 
           header("Location: mon_compte.php?id=".$_SESSION['id']);
           }
        }
     
   

}
catch(PDOException $e){
    echo 'Impossible de traiter les données. Erreur : '.$e->getMessage();
}
?>