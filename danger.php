<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';


?>

<div class="row justify-content-center mt-4">
    <div class="card border-dark mb-3">
        <div class="card-header">
            <div class="alert alert-warning" role="alert">
                <h3>Attention ! Si tu n'es pas un administrateur tu n'as pas le droit de venir ici ! </h3>
            </div>
        </div>

        <div class="card-body text-dark">
            <div class="row justify-content-center">
                <a href="index.php" class="btn btn-warning mt-4 mb-4" role="button" aria-pressed="true">Accueil</a>
            </div>
        </div>
    </div>
</div>


<?php
include 'footer.inc.php';
?>