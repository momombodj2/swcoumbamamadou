<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';


?>

<div class="row justify-content-center mt-4">
    <div class="card border-dark mb-3">
        <div class="card-header">
            <div class="alert alert-warning" role="alert">
                <h3>Attention ! Vous êtes dèja connecté, il faut vous déconnecter pour pouvoir vous reconnecter ! </h3>
            </div>
        </div>

        <div class="card-body text-dark">
            <div class="row justify-content-center">
                <a href="deco.php" class="btn btn-warning mt-4 mb-4" role="button" aria-pressed="true">Déconnexion</a>
            </div>
        </div>
    </div>
</div>


<?php
include 'footer.inc.php';
?>