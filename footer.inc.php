<script src="./js/jquery-3.5.1.slim.min.js"></script>
<script src="./js/popper.min.js"></script>
<script src="./js/bootstrap.min.js"></script>



</body>

<!-- création footer  -->
<?php
include 'connect.php';

$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);

if (isset($_SESSION['id']) and $_SESSION['id'] > 0) {
    $getid = intval($_SESSION['id']);
    $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();

    if ($userinfo['role_id'] == 1) {
?>

        <footer>

            <div class="p-3 bg-dark text-white">
                <div class="col-md-auto mb-1">
                    <a href="mon_compte.php" class="btn btn-outline-light" role="button" aria-pressed="true">Mon compte & Mes films</a>
                    <a href="administrateur.php" class="btn btn-outline-light" role="button" aria-pressed="true">Gérer la base de donnée</a>
                    <a href="utilisateur.php" class="btn btn-outline-light" role="button" aria-pressed="true">Rechercher un film </a>
                    <a href="index2.php" class="btn btn-outline-light" role="button" aria-pressed="true">Retour accueil</a>
                    <a href="deco.php" class="btn btn-outline-light" role="button" aria-pressed="true">Deconnexion</a>

                </div>
            </div>
        </footer>

    <?php
    } else if ($userinfo['role_id'] == 2) {
    ?>

        <footer>

            <div class="p-3 bg-dark text-white">
                <div class="col-md-auto mb-1">
                    <a href="mon_compte.php" class="btn btn-outline-light" role="button" aria-pressed="true">Mon compte & Mes films</a>
                    <a href="utilisateur.php" class="btn btn-outline-light" role="button" aria-pressed="true">Rechercher un film </a>
                    <a href="index2.php" class="btn btn-outline-light" role="button" aria-pressed="true">Retour accueil</a>
                    <a href="deco.php" class="btn btn-outline-light" role="button" aria-pressed="true">Deconnexion</a>

                </div>
            </div>
        </footer>



    <?php
    } }else {
?>
<footer>

<div class="p-3 bg-dark text-white">
    <div class="col-md-auto mb-1">
        <a href="mon_compte.php" class="btn btn-outline-light" role="button" aria-pressed="true">Mon compte</a>
        <a href="inscription.php" class="btn btn-outline-light" role="button" aria-pressed="true">S'inscrire</a>
        <a href="index2.php" class="btn btn-outline-light" role="button" aria-pressed="true">Retour accueil</a>
        <a href="deco.php" class="btn btn-outline-light" role="button" aria-pressed="true">Deconnexion</a>

    </div>
</div>
</footer>


<?php
    
}
?>