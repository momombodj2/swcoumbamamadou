<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';



$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);


if (isset($_SESSION['id'])) {
    $requser = $bdd->prepare("SELECT * FROM membres WHERE id = ?");
    $requser->execute(array($_SESSION['id']));
    $user = $requser->fetch();
    if (isset($_POST['newnom']) and !empty($_POST['newnom']) and $_POST['newnom'] != $user['nom']) {
        $newnom = htmlspecialchars($_POST['newnom']); //permet de sécuriser la variable (injection sql)
        $insertnom = $bdd->prepare("UPDATE membres SET nom = ? WHERE id = ?");
        $insertnom->execute(array($newnom, $_SESSION['id']));
        header('Location: mon_compte.php?id=' . $_SESSION['id']);
    }
    if (isset($_POST['newprenom']) and !empty($_POST['newprenom']) and $_POST['newprenom'] != $user['prenom']) {
        $newprenom = htmlspecialchars($_POST['newprenom']); //permet de sécuriser la variable (injection sql)
        $insertprenom = $bdd->prepare("UPDATE membres SET prenom = ? WHERE id = ?");
        $insertprenom->execute(array($newprenom, $_SESSION['id']));
        header('Location: mon_compte.php?id=' . $_SESSION['id']);
    }
    if (isset($_POST['newpseudo']) and !empty($_POST['newpseudo']) and $_POST['newpseudo'] != $user['pseudo']) {
        $newpseudo = htmlspecialchars($_POST['newpseudo']); //permet de sécuriser la variable (injection sql)
        $insertpseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id = ?");
        $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
        header('Location: mon_compte.php?id=' . $_SESSION['id']);
    }


    if (isset($_POST['newemail']) and !empty($_POST['newemail']) and $_POST['newemail'] != $user['email']) {
        $newemail = htmlspecialchars($_POST['newemail']);
        $insertemail = $bdd->prepare("UPDATE membres SET email = ? WHERE id = ?");
        $insertemail->execute(array($newemail, $_SESSION['id']));
        header('Location: mon_compte.php?id=' . $_SESSION['id']);
    }


    if (isset($_POST['newpassword1']) and !empty($_POST['newpassword1']) and isset($_POST['newpassword2']) and !empty($_POST['newpassword2'])) {
        $mdp1 = sha1($_POST['newpassword1']);
        $mdp2 = sha1($_POST['newpassword2']);
        if ($mdp1 == $mdp2) {
            $insertmdp = $bdd->prepare("UPDATE membres SET password = ? WHERE id = ?");
            $insertmdp->execute(array($mdp1, $_SESSION['id']));
            header('Location: mon_compte.php?id=' . $_SESSION['id']);
        } else {
            $msg = "Vos deux mdp ne correspondent pas !";
        }
    }
?>
    <html>

    <body>


        <div class="container">

            <div class="row justify-content-center mt-4">
                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header">
                        <h1>Page d'édition du profil</h1>
                    </div>
                </div>
            </div>


            <form method="POST" action="">

                <div class="row justify-content-center">
                    <div class="col-auto mb-4 mt-4">
                        <label>
                            <h4>Nom</h4>
                        </label>
                    </div>
                    <div class="col-4 mb-4 mt-4">
                        <input type="text" class="form-control" value="<?php echo $user['nom']; ?>" name="newnom">
                    </div>

                </div>


                <div class="row justify-content-center">
                    <div class="col-auto mb-4">
                        <label>
                            <h4>Prenom</h4>
                        </label>
                    </div>
                    <div class="col-4 mb-4">
                        <input type="text" class="form-control" value="<?php echo $user['prenom']; ?>" name="newprenom">
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-auto mb-4">
                        <label>
                            <h4>Pseudo</h4>
                        </label>
                    </div>
                    <div class="col-4 mb-4">
                        <input type="text" class="form-control" value="<?php echo $user['pseudo']; ?>" name="newpseudo">
                    </div>
                </div>



                <div class="row justify-content-center">
                    <div class="col-auto mb-4">
                        <label>
                            <h4>Email</h4>
                        </label>
                    </div>
                    <div class="col-4 mb-4">
                        <input type="email" class="form-control" value="<?php echo $user['email']; ?>" name="newemail">
                    </div>
                </div>


                <div class="row justify-content-center">
                    <div class="col-4 mb-4">
                        <input type="newpassword1" class="form-control" placeholder="Nouveau mot de passe" name="newpassword1">
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-4 mb-4">
                        <input type="newpassword2" class="form-control" placeholder="Confirmer le nouveau mot de passe" name="newpassword2">
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col-0 mb-4">
                        <button type="submit" class="btn btn-dark">Mise a jour profil</button>
                    </div>
                </div>



            </form>
        </div>

        <?php if (isset($msg)) {
            echo $msg;
        } ?>

    </body>

    </html>


<?php

} else {
    header("Location: connexion.php");
}

include 'footer.inc.php';
?>