<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';


$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);


if (isset($_SESSION['id']) and $_SESSION['id'] > 0) {
    $getid = intval($_SESSION['id']);
    $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();
?>

    <html>
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <div class="card border-dark mb-3" style="max-width: 30rem;">
                <div class="card-header">
                    <h1>Mon compte</h1>

                </div>
            </div>
        </div>
        <?php
        if (isset($_SESSION['id']) and $userinfo['id'] == $_SESSION['id']) {
        ?>
            <div class="col-md-auto">
                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header">
                        <a href="edition_profil.php" class="text-dark">
                            <h1>Editer mon profil</h1>
                        </a>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

    </div>


    <div class="container">
        <div class="row align-items-start">

            <div class="col">

                <div class="row justify-content-center mb-4 mt-2">
                    <div class="card border-dark mb-3" style="max-width: 30rem;">
                        <div class="card-header">
                            <h3>Mes informations</h3>
                        </div>
                    </div>
                </div>

                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header text-center">
                        <h5>Nom</h5>
                    </div>
                    <div class="card-body text-dark text-center">
                        <?php echo $userinfo['nom']; ?>
                    </div>
                </div>

                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header text-center">
                        <h5>Prenom</h5>
                    </div>
                    <div class="card-body text-dark text-center">
                        <?php echo $userinfo['prenom']; ?>
                    </div>
                </div>

                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header text-center">
                        <h5>Pseudo</h5>
                    </div>
                    <div class="card-body text-dark text-center">
                        <?php echo $userinfo['pseudo']; ?>
                    </div>
                </div>



                <div class="card border-dark mb-3" style="max-width: 30rem;">
                    <div class="card-header text-center">
                        <h5>E-mail</h5>
                    </div>
                    <div class="card-body text-dark text-center">
                        <?php echo $userinfo['email']; ?>
                    </div>
                </div>



        





            </div>


            <div class="col">
                <div class="row justify-content-center mb-4 mt-2">
                    <div class="card border-dark mb-3" style="max-width: 30rem;">
                        <div class="card-header">
                            <h3>Mes films favoris</h3>
                        </div>
                    </div>
                </div>

                <!-- ATTENTION SI PAS DE FILMS EN FAVORIS -->
                <?php if ($userinfo['id_f1_film'] == 0) {
                ?>

                    <div class="card border-dark mb-3" style="max-width: 30rem;">
                        <div class="card-body text-dark">
                            <div class="alert alert-info" role="alert">
                                <a href="utilisateur.php" class="text-dark">
                                    <h8>Pas de film en favoris ! Pour en ajouter cliquer ici </h8>
                                </a>

                            </div>
                        </div>
                    </div>


                    
                            <?php } else { ?>

                                <div class="card border-dark mb-3" style="max-width: 30rem;">
                                    <div class="card-body text-dark">
                                        <div class="row row-cols-1 row-cols-md-3">
                                            <!-- SQL REQUETE FILMS FAVORIS -->
                                            <?php
                                            $filmfav1 = 'film favori 1';
                                            $filmfav2 = 'film favori 2';
                                            $filmfav3 = 'film favori 3';
                                            $r = $bdd->prepare('SELECT * FROM film INNER JOIN membres ON film.id=id_f1_film WHERE film.id = ' . $userinfo['id_f1_film']);
                                            $r->execute();
                                            $ff1 = $r->fetch();
                                            $r->closeCursor();


                                            affichage_film_complet_fav($ff1['title'], $ff1['release_date'], $ff1['episode'], $ff1['opening'], $ff1['vote'], $filmfav1, $ff1['image']);

                                            //SQL REQUETE JEUX FAVORIS 2
                                            $r = $bdd->prepare('SELECT * FROM film INNER JOIN membres ON film.id=id_f2_film WHERE film.id = ' . $userinfo['id_f2_film']);
                                            $r->execute();
                                            $count = $r->rowCount();
                                            $ff2 = $r->fetch();
                                            $r->closeCursor();
                                            if ($count > 0) {
                                                affichage_film_complet_fav($ff2['title'], $ff2['release_date'], $ff2['episode'], $ff2['opening'], $ff2['vote'], $filmfav2, $ff2['image']);

                                                //SQL REQUETE JEUX FAVORIS 3
                                                $r = $bdd->prepare('SELECT * FROM film INNER JOIN membres ON film.id=id_f3_film WHERE film.id = ' . $userinfo['id_f3_film']);
                                            }
                                            $r->execute();
                                            $count = $r->rowCount();
                                            $ff3 = $r->fetch();
                                            $r->closeCursor();
                                            if ($count > 0) {
                                                affichage_film_complet_fav($ff3['title'], $ff3['release_date'], $ff3['episode'], $ff3['opening'], $ff3['vote'], $filmfav3, $ff3['image']);
                                            }
                                            ?>



                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                            </div>

                        </div></div></div></div></div>
                    </div>

    </html>

<?php
} else {
    header("Location: inscription.php");
}
include 'footer.inc.php';
?>

