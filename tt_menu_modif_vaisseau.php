<?php
    session_start();

    include 'connect.php';
    include 'fonction_php_mistake.php';
    

    $menu_modif = 2;
    $oldname = $_POST["ancien_vaisseau"];
    $newname = htmlspecialchars($_POST["nouveau_vaisseau"]);

    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //On insère les données reçues
        $insertvaisseau = $dbco->prepare("UPDATE starship SET class = ? WHERE class = ?");
        
        $insertvaisseau->execute(array($newname, $oldname));
        $insertvaisseau->closeCursor();
        
        succes($menu_modif);

    }
    catch(PDOException $e){
        if ($e->getCode() == 23000) {

            erreur($e->getCode(), $menu_modif);
           
        } else {
            header("Location: administrateur.php");
        }
    }
    ?>
