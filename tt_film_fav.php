<?php
session_start();
include 'connect.php';
include 'fonction_php_mistake.php';

$nom_film1 = htmlspecialchars($_POST["film_fav1"]);
$nom_film2 = htmlspecialchars($_POST["film_fav2"]);
$nom_film3 = htmlspecialchars($_POST["film_fav3"]);

$menu_util = 10;


try {
    $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

try {
    //On se connecte à la BDD
    $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
    $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



    $existe = $dbco->prepare("SELECT * FROM film WHERE title = ?");
    $existe->execute(array($nom_film1));
    $pexist = $existe->rowCount();
    $existe->closeCursor();


    if ($pexist == 1) {

        if (isset($nom_film1)) {


            $foundfilm = $dbco->prepare("SELECT * FROM film WHERE title = ?");
            $foundfilm->execute(array($nom_film1));
            $foundfilm2 = $foundfilm->fetch();
            $foundfilm->closeCursor();


            $insertfilmfav = $dbco->prepare("UPDATE membres SET id_f1_film = ? WHERE id = ?");
            $insertfilmfav->execute(array($foundfilm2['id'], $_SESSION['id']));
            $insertfilmfav->closeCursor();
        }

        if (isset($_POST["radio1"])) {
            if (isset($nom_film2)) {


                $foundfilm = $dbco->prepare("SELECT * FROM film WHERE title = ?");
                $foundfilm->execute(array($nom_film2));
                $foundfilm2 = $foundfilm->fetch();
                $foundfilm->closeCursor();


                $insertfilmfav = $dbco->prepare("UPDATE membres SET id_f2_film = ? WHERE id = ?");
                $insertfilmfav->execute(array($foundfilm2['id'], $_SESSION['id']));
                $insertfilmfav->closeCursor();
            }
        }
        if (isset($_POST["radio2"])) {
            if (isset($nom_film3)) {


                $foundfilm = $dbco->prepare("SELECT * FROM film WHERE title = ?");
                $foundfilm->execute(array($nom_film3));
                $foundfilm2 = $foundfilm->fetch();
                $foundfilm->closeCursor();


                $insertfilmfav = $dbco->prepare("UPDATE membres SET id_f3_film = ? WHERE id = ?");
                $insertfilmfav->execute(array($foundfilm2['id'], $_SESSION['id']));
                $insertfilmfav->closeCursor();
            }
        }

        header("Location: mon_compte.php?id=" . $_SESSION['id']);
    } else {
        erreur($menu_util, $menu_util);
    }
} catch (PDOException $e) {
    echo 'Erreur : ' . $e->getMessage();
}
