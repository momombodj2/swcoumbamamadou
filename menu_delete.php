<?php
session_start();

include 'header.inc.php';
include 'fonction_php_mistake.php';
include 'navbar.inc.php';
insntconnected_admin();

?>

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Menu suppression</h1>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row justify-content-center mt-4">

            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Suppression planète</h3>
                    </div>
                    <form action="tt_menu_delete_planete.php" method="POST">
                        <div class="card-body text-dark">

                            <div class="row align-items-center">

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nom de la planète à supprimer" required name="supprime_planete">
                                </div>


                                <!-- Début POP UP -->
                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal">
                                    Supprimer
                                </button>

                                <!-- CONTENU POP UP -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">ATTENTION SUPPRESSION</h5>
                                                <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Êtes vous sur de vouloir supprimer cette planete ? La suppression est irréversible ! </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                                                <button type="Submit" class="btn btn-danger">Supprimer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Suppresion vaisseau</h3>
                    </div>
                    <form action="tt_menu_delete_vaisseau.php" method="POST">
                        <div class="card-body text-dark">
                            <div class="row align-items-center">

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nom du vaisseau à supprimer" required name="supprime_vaisseau">
                                </div>

                                <!-- Début POP UP -->
                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal2">
                                    Supprimer
                                </button>

                                <!-- CONTENU POP UP -->
                                <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">ATTENTION SUPPRESSION</h5>
                                                <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Êtes vous sur de vouloir supprimer ce vaisseau ? La suppression est irréversible ! </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                                                <button type="Submit" class="btn btn-danger">Supprimer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="card border-dark mb-3 w-100">

            <div class="card-header">
                <h3>Suppresion film </h3>
            </div>
            <form action="tt_menu_delete_film.php" method="POST">
                <div class="card-body text-dark">

                    <div class="row">

                        <div class="col mt-2 mb-2">
                            <input type="text" class="form-control" placeholder="Nom du film à supprimer" required name="supprime_film">
                        </div>

                        <!-- Début POP UP -->
                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal3">
                            Supprimer
                        </button>

                        <!-- CONTENU POP UP -->
                        <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">ATTENTION SUPPRESSION</h5>
                                        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Êtes vous sur de vouloir supprimer ce film ? La suppression est irréversible ! </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                                        <button type="Submit" class="btn btn-danger">Supprimer</button>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
include 'footer.inc.php';
?>