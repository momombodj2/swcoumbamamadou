<?php
if (isset($_SESSION['id'])) {
?>

    <ul class="nav nav-pills nav-fill ">
        <li class="nav-item">
            <a href="index.php" class="btn btn-dark btn-lg active btn-block mb-4" role="button" aria-pressed="true">Accueil</a>
        </li>}
    </ul>

<?php
} else { ?>
    <ul class="nav nav-pills nav-fill ">
        <li class="nav-item">
            <a href="index.php" class="btn btn-dark btn-lg active btn-block mb-4" role="button" aria-pressed="true">Accueil</a>
        </li>
        <li class="nav-item">
            <a href="connexion.php" class="btn btn-dark btn-lg active btn-block mb-4" role="button" aria-pressed="true">Connexion</a>
        </li>
        <li class="nav-item">
            <a href="inscription.php" class="btn btn-dark btn-lg active btn-block mb-4" role="button" aria-pressed="true">Inscription</a>
        </li>

    </ul>
<?php } ?>