<?php
session_start();

include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';
insntconnected_admin();

try {
    $bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

?>

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Menu création</h1>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row justify-content-center mt-4">

            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Création planète</h3>
                    </div>
                    <form action="tt_menu_crea_planete.php" method="POST">
                        <div class="card-body text-dark">

                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nom" required name="nom_planete">
                                </div>

                                <div class="col">
                                    <button type="submit" class="btn btn-dark">Enregistrer</button>
                                </div>


                            </div>

                        </div>
                    </form>
                </div>
            </div>



            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Création vaisseau</h3>
                    </div>
                    <form action="tt_menu_crea_vaisseau.php" method="POST">
                        <div class="card-body text-dark">

                            <div class="row align-items-center">

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nom" required name="nom_starship">
                                </div>

                                <div class="col">
                                    <button type="submit" class="btn btn-dark">Enregistrer</button>
                                </div>


                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="card border-dark mb-3 w-100">

            <div class="card-header">
                <h3>Création film</h3>
            </div>

            <form action="tt_menu_crea_film.php" method="POST" >
                <div class="card-body text-dark">

                    <div class="row align-items-center">

                        <div class="row w-50">

                            <div class="col">
                                <span class="badge badge-pill badge-light ml-auto">Nom du film </span>
                                <input type="text" class="form-control" placeholder="Inserer le film" required name="nom_film">
                            </div>
                        </div>
                    



                    </div>
                   
                    <div class="row align-items-center">

                        <div class="row mt-4 w-100">

                            <div class="col">
                                <button type="submit" class="btn btn-dark w-100">Enregistrer</button>
                            </div>


                        </div>
                    </div>
                </div>
        
            </form>
        </div>
    </div>
</div>

<?php
include 'footer.inc.php';
?>