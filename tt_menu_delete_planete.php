<?php
    session_start();

    include 'connect.php';
    include 'fonction_php_mistake.php';
    $menu_delete = 3;
    $planettodelete = $_POST["supprime_planete"];
    $menu_delete_planete = 2;

    try {
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
    
    $existe = $dbco->prepare("SELECT * FROM planet WHERE name = ?");
    $existe->execute(array($planettodelete));
    $pexist = $existe->rowCount();

    if ($pexist == 1) {

    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //On supprime la donnée reçue
        $insertplanet = $dbco->prepare("DELETE FROM planet WHERE name = ?");
        
        $insertplanet->execute(array($planettodelete));
        $insertplanet->closeCursor();

        
        succes($menu_delete);
    }
    catch(PDOException $e){
        
        if ($e->getCode() == 23000) {

        erreur_delete_exist($menu_delete_planet);
       
    } else {
        header("Location: administrateur.php");
    }
    }}
    else { erreur_delete();}
   

    ?>
