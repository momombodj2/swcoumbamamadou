<?php
session_start();

include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';

insntconnected_user();


?>


<div class="container">

    <?php include 'menu_recherche.php'; ?>

    <div class="col">
        <div class="card border-dark mb-3 w-100">
           
            <div class="card-body text-dark">

                <?php affichage_top_3(); ?>

                <div class="col mt-3">
                    <a class="btn btn-outline-dark w-100" href="classement_vote.php" role="button" aria-pressed="true">Classement des films</a>
                </div>
                <div class="card border-dark mt-3 mb-3 w-100">
                    <div class="card-header">

                        <h3 class="text-center">Vote</h3>
                    </div>
                    <div class="card-body text-dark">
                        <form action="tt_vote.php" method="POST">


                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control w-100" placeholder="Nom du film" required name="vote">
                                </div>

                                <div class="col">
                                    <button type="submit" class="btn btn-dark w-100">Voter</button>
                                </div>


                            </div>


                        </form>

                    </div>
                </div>

                <div class="card border-dark mt-3 mb-3 w-100">
                    <div class="card-header">

                        <h3 class="text-center">Ajout d'un film à mes favoris</h3>
                    </div>
                    <div class="card-body text-dark">
                        <form action="tt_film_fav.php" method="POST">


                            <div class="row">
                                <div class="col mt-2">
                                    <input type="text" class="form-control w-100" placeholder="Nom du premier film" required name="film_fav1">
                                </div>


                            </div>
                            <div class="row">
                                <div class="col mt-2">
                                    <span class="badge badge-pill badge-light">Optionnel, cocher pour ajouter votre deuxième film favori <input type="checkbox" id="radio1" name="radio1" value="OUI"> </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col mt-2">
                                    <input type="text" class="form-control w-100" placeholder="Nom du deuxième film" name="film_fav2">
                                </div>


                            </div>

                            <div class="row">
                                <div class="col mt-2">
                                    <span class="badge badge-pill badge-light">Optionnel, cocher pour ajouter votre troisième film favori <input type="checkbox" id="radio2" name="radio2" value="OUI"> </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col mt-2 mb-2">
                                    <input type="text" class="form-control w-100" placeholder="Nom du troisième film" name="film_fav3">
                                </div>


                            </div>
                            <div class="col mt-2">
                                <button type="submit" class="btn btn-dark w-100">Ajouter à mes favoris</button>
                            </div>
                        </form>

                    </div>
                </div>





            </div>
        </div>
    </div>
</div>


</div>


<?php
include 'footer.inc.php';
?>