<?php
session_start();

include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';

try {
    $bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}
insntconnected_admin();

?>

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Menu modification</h1>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row justify-content-center mt-4">

            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Modification vaisseau</h3>
                    </div>
                    <form action="tt_menu_modif_vaisseau.php" method="POST">
                        <div class="card-body text-dark">

                            <div class="row align-items-center">
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Ancien vaisseau" required name="ancien_vaisseau">
                                </div>

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nouveau vaisseau" required name="nouveau_vaisseau">
                                </div>

                                <div class="col">
                                    <button type="Submit" class="btn btn-dark">Modifier</button>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>



            <div class="col">

                <div class="card border-dark mb-3 w-100">

                    <div class="card-header">
                        <h3>Modification planète</h3>
                    </div>
                    <form action="tt_menu_modif_planete.php" method="POST">
                        <div class="card-body text-dark">
                            <div class="row align-items-center">

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Ancien nom" required name="ancien_planete">
                                </div>

                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Nouveau nom" required name="nouveau_planete">
                                </div>

                                <div class="col">
                                    <button type="Submit" class="btn btn-dark">Modifier</button>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="card border-dark mb-3 w-100">

            <div class="card-header">
                <h3>Modification film complet</h3>
            </div>

            <form action="tt_menu_modif_film.php" method="POST">
                <div class="card-body text-dark">

                    <div class="row align-items-center">

                        <div class="row w-50">

                            <div class="col">
                                <span class="badge badge-pill badge-light ml-auto">Ancien nom du film </span>
                                <input type="text" class="form-control" placeholder="Inserer le film" required name="old_nom_film">
                            </div>

                            <div class="col">
                                <span class="badge badge-pill badge-light ml-auto">Nouveau nom film </span>
                                <input type="text" class="form-control" placeholder="Inserer le film" required name="new_nom_film">
                            </div>

                        </div>

                    </div>


                    <div class="row align-items-center">

                        <div class="row w-100">

                            <div class="col">
                                <span class="badge badge-pill badge-light ml-auto">Nouvelle description du film </span>
                                <input type="text" class="form-control" placeholder="Inserer le texte" required name="opening">
                            </div>


                        </div>
                    </div>
                    <div class="row align-items-center">

                        <div class="row mt-4 w-100">

                            <div class="col">
                                <button type="submit" class="btn btn-dark w-100">Modifier</button>
                            </div>


                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
include 'footer.inc.php';
?>