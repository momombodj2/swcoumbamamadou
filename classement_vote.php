<?php
session_start();

include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';


$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);

$articles = $bdd->prepare('SELECT * FROM film ORDER BY film.vote DESC');
$articles->execute();

?>
<div class="container">

    <?php include 'menu_recherche.php'; ?>
    <div class="col">
        <div class="card-body text-dark">

            <div class="card border-dark mb-3 w-100">
                <div class="card-header">
                    <div class="col">
                        <h3 class="text-center">Classement de tous les films</h3>
                    </div>

                </div>

                <div class="card-body text-dark">
                    <div class="container">



                        <?php if ($articles->rowCount() > 0) {

                            $classement = 1;
                            while ($a = $articles->fetch()) {

                                affichage_film_complet_classement($a['title'], $a['release_date'], $a['episode'], $a['opening'], $a['vote'], $classement, $a['image']);
                                $classement++;
                            }
                        } else { ?>
                            Aucun resultat pour: <?= $q ?>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN BARRE DE RECHERCHE -->

</div>




<?php

include 'footer.inc.php';
?>