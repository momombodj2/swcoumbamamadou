<?php
session_start();

include 'connect.php';
include 'fonction_php_mistake.php';

$nom = htmlspecialchars($_POST["nom_planete"]);
$menu_crea = 1;

try {
    //On se connecte à la BDD
    $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
    $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //On insère les données reçues
    $sth = $dbco->prepare("
            INSERT INTO planet(name)
            VALUES(:nom)");

    $sth->bindParam(':nom', $nom);

    $sth->execute();
    $sth->closeCursor();

    succes($menu_crea);
    
} catch (PDOException $e) {

    if ($e->getCode() == 23000) {

        erreur($e->getCode(), $menu_crea);
       
    } else {
        header("Location: administrateur.php");
    }
}
?>