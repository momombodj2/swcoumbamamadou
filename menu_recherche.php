<div class="row">
    <div class="col">
        <div class="card border-dark mb-3 w-100">
            <div class="">
                <h2 class="text-center">Menu recherche</h2>
            </div>
            <div class="card-body text-dark">
                <div class="row">
                   
                    <div class="col">
                        <a class="btn btn-outline-dark mb-2" href="recherche_name.php" role="button" aria-pressed="true">Recherche par nom de film</a>
                    </div>

                        <a class="btn btn-outline-dark mb-2 w-100" href="index.php" role="button" aria-pressed="true">Retour </br> accueil</a>
                    </div>
                    <div class="col">
                        <a class="btn btn-outline-dark" href="utilisateur.php" role="button" aria-pressed="true">Retour menu utilisateur</a>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <a href="affichage_all_film.php" class="text-dark">
                    <h2 class="text-center">Nos films favoris</h2>
                </a>
            </div>

        </div>
    </div>
</div>