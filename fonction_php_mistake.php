<?php

function succes($arg_1) // Gestion des erreurs pour admin gérant BDD
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

        echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Excellent ! </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-success" role="alert">';
                
                if($arg_1 == 1 || $arg_1 == 2)
                { echo'<h3>Insertion en base de donnée réussie ! </h3>';}
                
                elseif($arg_1 == 3)
               { echo'<h3>Suppression de la base de donnée réussie ! </h3>';}

              
                    
               echo'</div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">';
                if($arg_1 == 1)
                { echo'<a href="menu_creation.php" class="btn btn-success mt-4 mb-4" role="button" aria-pressed="true">Retour menu creation</a>';}
             
                elseif($arg_1 == 2)
                { echo'<a href="menu_modification.php" class="btn btn-success mt-4 mb-4" role="button" aria-pressed="true">Retour menu modification</a>'; }
             
                elseif($arg_1 == 3)
                {echo'<a href="menu_delete.php" class="btn btn-success mt-4 mb-4" role="button" aria-pressed="true">Retour menu suppression</a>'; }

               
                
             echo'
                </div>

                <div class="row justify-content-center">
                    <a href="administrateur.php" class="btn btn-success mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }

function erreur($arg_1, $arg_2) // Gestion des erreurs pour admin gérant BDD
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page de gestion des erreurs </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-danger" role="alert">';
                if($arg_1 == 23000)
                {
                    echo '<h3>Insertion impossible, la donnée saisie est dèja existante en base de donnée ! </h3>';
                } 
                
                elseif($arg_1 == 10)
                { echo'<h3>Le film ne se situe pas dans la base de donnée actuelle.. Désolé ! </h3>';}
                echo '
                </div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">';
                if($arg_2 == 1)
                   { echo'<a href="menu_creation.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu creation</a>';}
                
                   elseif($arg_2 == 2)
                   { echo'<a href="menu_modification.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu modification</a>'; }

                   elseif($arg_1 == 10)
                   {echo'<a href="utilisateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu utilisateur</a>'; }
                
                
                echo'    </div>

               

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }


 function erreur_delete() // Gestion des erreurs pour admin gérant BDD
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page de gestion des erreurs </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-danger" role="alert">
                
                
                 <h3>Suppression impossible, la donnée saisie est invalide car elle ne se trouve pas en base de donnée ! </h3>
                
                
                </div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">

                <a href="menu_delete.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu suppression</a>
                
                </div>

                <div class="row justify-content-center">
                    <a href="administrateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }

 function erreur_delete_exist($arg_1) // Gestion des erreurs pour admin gérant BDD
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page de gestion des erreurs </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-danger" role="alert">';
                if($arg_1 == 1)
                {
                    echo '<h3>Suppresion de la planete impossible ! </h3>';}
               
                                    
                echo' 
                </div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">
                <a href="menu_delete.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu suppression</a>
                   </div>
                
                
                             
                <div class="row justify-content-center">
                    <a href="administrateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }
   
 function affichage_top_3() // affiche le top 3 des films
 {
    include 'connect.php';

    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME",$LOGIN,$MDP);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // On récupère un contenu top 3 classement dans la BDD
        $reponse = $dbco->prepare('SELECT * FROM film ORDER BY vote DESC LIMIT 3');
        $reponse->execute();
        // On affiche chaque entrée une à une
        $donnees = $reponse->fetch(); 

	//On récupère le nombre de vote total
	$reponse2 = $dbco->prepare('SELECT SUM(vote) as vote from film');
	$reponse2->execute();
	$donnees2 = $reponse2->fetch();

        echo '
        <div class="card-deck">
<div class="row">
<div class="col-sm">
        <div class="card" style="max-width: 15rem;">
          <div class="card-body">
            <h5 class="card-title">
           
            </h5>
            <p class="card-text"> <p class="text-success"> Vous trouverez le classement des trois films ayant obtenu le plus de vote depuis la création du site !  </br> </p>  </p>
            <p class="card-text"><small class="text-muted"> Nombre de vote total actuel : '; echo $donnees2['vote'];
                 
            echo ' </small></p>
          </div></div>
        </div>

<div class="col-sm">
        <div class="card" style="max-width: 15rem;">
          <img src="image/';if(isset($donnees['image'])) {echo $donnees['image'];} echo'" class="card-img-top mt-2" alt="';echo $donnees['opening']; echo'">';
        echo '<div class="card-body">
            <h5 class="card-title">'; echo $donnees['title'];
            echo'
            </h5>
            <p class="card-text"> <p class="text-info"> PREMIER </br> </p>'; echo $donnees['opening']; echo'</p>
            <p class="card-text"><small class="text-muted"> Nombre de vote actuel : ';
             echo $donnees['vote']; 
             $donnees = $reponse->fetch(); 
             echo'</small></p>
          </div></div>
        </div>
<div class="col-sm">
        <div class="card ml-6" style="max-width: 15rem;">
        <img src="image/';if(isset($donnees['image'])) {echo $donnees['image'];} echo'" class="card-img-top mt-2" alt="';echo $donnees['opening']; echo'">';
        echo '<div class="card-body">
            <h5 class="card-title">'; echo $donnees['title'];
            echo'</h5>
            <p class="card-text"> <p class="text-info"> DEUXIEME </br> </p> '; echo $donnees['opening']; echo'</p>
            <p class="card-text"><small class="text-muted">Nombre de vote actuel : ';
            echo $donnees['vote']; 
            $donnees = $reponse->fetch(); 
            echo'</small></p>
          </div>
        </div></div>
<div class="col-sm">
        <div class="card" style="max-width: 15rem;">
        <img src="image/';if(isset($donnees['image'])) {echo $donnees['image'];} echo'" class="card-img-top mt-2" alt="';echo $donnees['opening']; echo'">';
        echo '<div class="card-body">
            <h5 class="card-title">';echo $donnees['title'];
            echo'</h5>
            <p class="card-text"> <p class="text-info"> TROISIEME </br> </p> '; echo $donnees['opening']; echo'</p>
            <p class="card-text"><small class="text-muted">Nombre de vote actuel : ';
            echo $donnees['vote']; 
            $reponse->closeCursor(); 
            echo'</small></p>
          </div></div>
        </div></div>
      </div>
      ';

    }


    catch(PDOException $e)


{
echo 'Erreur : ' . $e->getMessage();

}
 }


 function isvoted($name) // Page vote pris en compte pour admin gérant BDD
{
    
    include 'header.inc.php';
    include 'navbar.inc.php';
    include 'connect.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page du classement actuel </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-info" role="alert">
                
                
                 <h3>Votre vote pour le film '; echo $name; echo' a été pris en compte ! </h3>
                 <h5>Vous trouverez le top 3 des films ci dessous </h5>
                
                
                </div>';

                
            affichage_top_3(); // utilisation fonction affichage

            echo' <div class="card-footer bg-transparent border-dark">
                <div class="row justify-content-center">

                <a href="utilisateur.php" class="btn btn-info mt-4 mb-4" role="button" aria-pressed="true">Retour menu utilisateur</a>
                
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }
    



 function affichage_film_complet($title, $release_date, $episode, $opening, $image, $vote)

 {
    echo '
    <div class="col mt-2 mb-2 style="max-width: 30rem;">
    <div class="card h-100">
        <img src="image/';if(isset($image)) {echo $image;} echo'" class="card-img-top mt-2" alt="'; echo'">';
        echo '<div class="card-body">';
            


        echo '<h6 class="text-info">Nom du film : </h6>'; echo $title; //Affiche le nom du film
        echo '</br><h7 class="text-info">Date de sortie : </h7>'; echo $release_date;
       

        echo '</br><h7 class="text-info">Numero episode : </h7>'; echo $episode;
        
        

        echo'<p class="card-text">

            <h10 class="text-info">Description : </h10>'; echo $opening; //Affiche la description du film
            echo '<p class="card-text"><small class="text-muted">Nombre de vote actuel : ';
            echo $vote; 
            echo '</small></p>

        </div>
    </div>
</div>
</br>';


 }

 function isvoted_error($name) // Page vote pris en compte pour admin gérant BDD
 {
     
     include 'header.inc.php';
     include 'navbar.inc.php';
     include 'connect.php';
 
 echo'
         <div class="row justify-content-center mt-4">
             <div class="card border-dark mb-3" style="max-width: 50rem;">
                 <div class="card-header">
                     <h1>Page du classement actuel </h1>
                 </div>
             </div>
         </div>
 
 
 
         <div class="row justify-content-center">
             
         <div class="row justify-content-center mt-4">
         <div class="card border-dark mb-4">
            
                 <div class="alert alert-danger" role="alert">
                 
                 
                  <h3>Vous avez déja voté pour  '; echo $name; echo' lors de cette connexion, désolé ! </h3>
                  <h5>Vous trouverez le top 3 des films ci dessous </h5>
                 
                 
                 </div>';
 
                 
             affichage_top_3(); // utilisation fonction affichage
 
             echo' <div class="card-footer bg-transparent border-dark">
                 <div class="row justify-content-center">
 
                 <a href="utilisateur.php" class="btn btn-info mt-4 mb-4" role="button" aria-pressed="true">Retour menu utilisateur</a>
                 
                 </div>
 
             </div>
         </div>
     </div>
 
         </div>';
 
 
         include 'footer.inc.php';
  }

  function affichage_personnage_visiteur($name, $height, $mass)

  {
    echo '
    <div class="col mt-2 mb-2">
    <div class="card h-100">
        <img src="image/" class="card-img-top mt-2" alt="'; echo'">';
         echo '<div class="card-body">';
             
 
 
             echo '<h6 class="text-info">Nom du personnage : </h6>'; echo $name; //Affiche le nom du personnage 
             echo '</br><h7 class="text-info">Taille : </h7>'; echo $height;
             
 
             echo '</br><h7 class="text-info">Poids : </h7>'; echo $mass;
                 
echo' 
         </div>
     </div>
 </div>
 </br>';
 
 
  }
  

  function affichage_film_complet_fav($title, $release_date, $episode, $opening, $vote , $filmfav, $image)

 {
    echo '
    <div class="col mt-2 mb-2">
    <div class="card h-100 w-100">
        <img src="image/';if(isset($image)) {echo $image;} echo'" class="card-img-top mt-2" alt="';echo $opening; echo'">';
        echo '<div class="card-body">';
            


            echo '<h6 class="text-info">Nom du ';echo $filmfav; echo' : </h6>'; echo $title; //Affiche le nom du film
            echo '</br><h7 class="text-info">Date de sortie : </h7>'; echo $release_date;
            

            echo '</br><h7 class="text-info">Numero episode : </h7>'; echo $episode;
            
           

            echo'<p class="card-text">

                <h10 class="text-info">Description : </h10>'; echo $opening; //Affiche la description du film
                echo '<p class="card-text"><small class="text-muted">Nombre de vote actuel : ';
                echo $vote; 
                echo '</small></p>

        </div>
    </div>
</div>
</br>';


 }

 function affichage_film_complet_classement($title, $release_date, $episode, $opening, $vote,$classement, $image)

 {
    echo '
    <div class="col mt-2 mb-2">
    <div class="card h-100 w-100">
        <img src="image/';if(isset($image)) {echo $image;} echo'" class="card-img-top mt-2" alt="';echo $opening; echo'">';
        echo '<div class="card-body">';
            

            echo '<h4 class="text-info">Classement : ';echo $classement; echo '</h4>';
            echo '<h6 class="text-info">Nom du film : </h6>'; echo $title; //Affiche le nom du film 
            echo '</br><h7 class="text-info">Date de sortie : </h7>'; echo $release_date;
            

            echo '</br><h7 class="text-info">Numero episode : </h7>'; echo $episode;
            
            echo'<p class="card-text">

                <h10 class="text-info">Description : </h10>'; echo $opening; //Affiche la description du film
                echo '<p class="card-text"><small class="text-muted">Nombre de vote actuel : ';
                echo $vote; 
                echo '</small></p>

        </div>
    </div>
</div>
</br>';


 }

 /*function erreur_image_format() 
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page de gestion des erreurs </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-danger" role="alert">
                
                
                 <h3>Insertion impossible, mauvais format de la photo désolé ! </h3>
                
                
                </div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">

                <a href="menu_creation.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu creation</a>
                
                </div>

                <div class="row justify-content-center">
                    <a href="administrateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }

 function erreur_image_taille() // Gestion des erreurs pour admin gérant BDD
 {
     
     include 'header.inc.php';
         include 'navbar.inc.php';
 
 echo'
         <div class="row justify-content-center mt-4">
             <div class="card border-dark mb-3" style="max-width: 50rem;">
                 <div class="card-header">
                     <h1>Page de gestion des erreurs </h1>
                 </div>
             </div>
         </div>
 
 
 
         <div class="row justify-content-center">
             
         <div class="row justify-content-center mt-4">
         <div class="card border-dark mb-4">
            
                 <div class="alert alert-danger" role="alert">
                 
                 
                  <h3>Insertion impossible, taille trop grande pour la photo désolé ! </h3>
                 
                 
                 </div>
             
     
             <div class="card-body text-dark">
                 <div class="row justify-content-center">
 
                 <a href="menu_creation.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu creation</a>
                 
                 </div>
 
                 <div class="row justify-content-center">
                     <a href="administrateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                 </div>
 
             </div>
         </div>
     </div>
 
         </div>';
 
 
         include 'footer.inc.php';
  }


  function erreur_image() // Gestion des erreurs pour admin gérant BDD
{
    
    include 'header.inc.php';
        include 'navbar.inc.php';

echo'
        <div class="row justify-content-center mt-4">
            <div class="card border-dark mb-3" style="max-width: 50rem;">
                <div class="card-header">
                    <h1>Page de gestion des erreurs </h1>
                </div>
            </div>
        </div>



        <div class="row justify-content-center">
            
        <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-4">
           
                <div class="alert alert-danger" role="alert">
                
                
                 <h3>Insertion impossible de la photo désolé ! </h3>
                
                
                </div>
            
    
            <div class="card-body text-dark">
                <div class="row justify-content-center">

                <a href="menu_creation.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu creation</a>
                
                </div>

                <div class="row justify-content-center">
                    <a href="administrateur.php" class="btn btn-danger mt-4 mb-4" role="button" aria-pressed="true">Retour menu gérer la base donnée</a>
                </div>

            </div>
        </div>
    </div>

        </div>';


        include 'footer.inc.php';
 }

/*function insertimage($nom_film, $dbco, $menu_crea){

    $existe = $dbco->prepare("SELECT * FROM film WHERE title = ?");
    $existe->execute(array($nom_film));
    $pexist = $existe->fetch();



   
 if (isset($_FILES['image']) and !empty($_FILES['image']['name'])) {
    $tailleMax = 2097152;
    $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
    if ($_FILES['image']['size'] <= $tailleMax) {
        $extensionUpload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1));
        if (in_array($extensionUpload, $extensionsValides)) {
            $chemin = "image/" . $pexist['id'] . "." . $extensionUpload;
            $resultat = move_uploaded_file($_FILES['image']['tmp_name'], $chemin);
            if ($resultat) {
                $updateavatar = $dbco->prepare('UPDATE film SET image = :image WHERE id = :id');
                $updateavatar->execute(array(
                    'image' => $pexist['id'] . "." . $extensionUpload,
                    'id' => $pexist['id']
                ));
                succes($menu_crea);
            } else {
                erreur_image();
            }
        } else {
            erreur_image_format();
        }
    } else {
        erreur_image_taille();
    }
}

}*/

function insntconnected_admin()
{
    if($_SESSION['role_id'] == 1)
    {   }
    else{

        header("Location: danger.php");
    }
}

function insntconnected_user()
{
    if($_SESSION['role_id'] == 2)
    {   }
    else{

        header("Location: danger2.php");
    }
}

    ?>