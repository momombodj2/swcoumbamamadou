<?php
session_start();

include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';
insntconnected_user();
$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);

$articles = $bdd->prepare('SELECT * FROM film ');
$articles->execute();

if (isset($_GET['q']) and !empty($_GET['q'])) { // permet la recherche en BDD
    $q = htmlspecialchars($_GET['q']);
    $articles = $bdd->prepare('SELECT * FROM film WHERE film.title LIKE "%' . $q . '%" ORDER BY film.title ASC');
    $articles->execute();
} ?>
<div class="container">

    <?php include 'menu_recherche.php'; ?>
    <div class="col">
        <div class="card-body text-dark">

            <div class="card border-dark mb-3 w-100">
                <div class="card-header">
                    <div class="col">
                        <h3 class="text-center">Recherche sur le nom du film</h3>
                    </div>
                    <div class="col">
                        <form method="GET">
                            <input type="search" name="q" placeholder="Recherche..." />
                            <input type="submit" value="Valider" />
                        </form>
                    </div>
                </div>

                <div class="card-body text-dark">
                    <div class="container">



                        <?php if ($articles->rowCount() > 0) { ?>


                            <?php while ($a = $articles->fetch()) { 

                                affichage_film_complet($a['title'], $a['release_date'], $a['episode'], $a['opening'], $a['image'], $a['vote']);
                            }
                        } else { ?>
                            Aucun résultat pour: <?= $q ?>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN BARRE DE RECHERCHE -->

</div>




<?php

include 'footer.inc.php';
?>