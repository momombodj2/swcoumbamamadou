<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';
insntconnected_user();
$bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);

$articles = $bdd->prepare('SELECT * FROM film');
$articles->execute();


?>
<div class="card border-dark mb-3">
    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Librairie de nos films</h1>
            </div>
        </div>
    </div>



    <div class="row justify-content-center">

        <?php if ($articles->rowCount() > 0) { ?>


        <?php while ($a = $articles->fetch()) {

                affichage_jeu_complet($a['title'], $a['release_date'], $a['episode'], $a['opening'], $a['image'], $a['vote']);
            }
        } else {
            header("Location: index.php");
        } ?>


    </div>
</div>
<?php
include 'footer.inc.php';
?>