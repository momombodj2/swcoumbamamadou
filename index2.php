<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';
include 'connect.php';
include 'fonction_php_mistake.php';


try {
    // On se connecte à MySQL
    $bdd = new PDO("mysql:host=$SERVEUR;dbname=$DBNAME", $LOGIN, $MDP);
} catch (Exception $e) {
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : ' . $e->getMessage());
}


?>



    <div class="row justify-content-center mt-2 mb-3">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h3>BIENVENUE SUR STARWARS SITE</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col">


                <div class="card">
                    <div class="card-header">
                        Quelques films
                    </div>
                    <?php // On récupère un contenu aléatoire dans la BDD
            $reponse = $bdd->prepare('SELECT DISTINCT * FROM film ');
            $reponse->execute();
            $nombrefilm=$reponse->rowCount();
            // On affiche chaque entrée une à une

            ?>
            <div class="col">
                <div class="row row-cols-1 row-cols-md-1">

                    <?php
                    for ($i = 0; $i <$nombrefilm; $i++) {
                        $donnees = $reponse->fetch();
                        affichage_film_complet($donnees['title'], $donnees['release_date'], $donnees['episode'], $donnees['opening'], $donnees['image'], $donnees['vote']);
                    ?>
                  
                    
                    <?php } ?>



                </div>
            </div>

                </div>
            </div>

            <?php // On récupère un contenu aléatoire dans la BDD
            $reponse = $bdd->prepare('SELECT DISTINCT * FROM people ');
            $reponse->execute();
            $nombrepersonnage=$reponse->rowCount();
            // On affiche chaque entrée une à une

            ?>
            <div class="col">
                <div class="row row-cols-1 row-cols-md-1">

                    <?php
                    for ($i = 0; $i <$nombrepersonnage; $i++) {
                        $donnees = $reponse->fetch();
                        affichage_personnage_visiteur($donnees['name'], $donnees['height'], $donnees['mass']);
                    ?>
                  
                    
                    <?php } ?>



                </div>
            </div>
        </div>

    </div>





    <?php
    include 'footer.inc.php';
    ?>