<?php
  session_start();
  include 'header.inc.php';
  include 'navbar.inc.php';

  if(isset($_SESSION['id'])) {

    header("Location: isregister.php");} // si utilisateur dèja connecté on le renvoie vers une page lui indiquant qu'il est connecté
else {
?>

<div class="container">

    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Page d'inscription</h1>
            </div>
        </div>
    </div>


    <form action="tt_inscription.php" method="POST">

        <div class="row justify-content-center">
            <div class="col-4 mb-4 mt-4">
                <input type="text" class="form-control" placeholder="Nom" required name="nom">
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-4 mb-4">
                <input type="text" class="form-control" placeholder="Prenom" required name="prenom">
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-4 mb-4">
                <input type="text" class="form-control" placeholder="Pseudo" required name="pseudo">
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-4 mb-4">
                <input type="email" class="form-control" placeholder="Adresse email" required name="email">
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-4 mb-4">
                <input type="password" class="form-control" placeholder="Mot de passe" required name="password">
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-0 mb-4">
                <button type="submit" class="btn btn-dark">Enregistrer</button>
            </div>
        </div>



    </form>
</div>



<?php
}
  include 'footer.inc.php';
?>