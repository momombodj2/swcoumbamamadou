<?php
session_start();
include 'header.inc.php';
include 'navbar.inc.php';



if (isset($_SESSION['id'])) {

    header("Location: isconnect.php");
} // si utilisateur dèja connecté on le renvoie vers une page lui indiquant qu'il est connecté
else {
?>

    <div class="row justify-content-center mt-4">
        <div class="card border-dark mb-3" style="max-width: 30rem;">
            <div class="card-header">
                <h1>Page de connexion</h1>
            </div>
        </div>
    </div>



    <div class="row justify-content-center">
        <form action="tt_connexion.php" method="POST">
            <div class="form-group">
                <label for="exampleInputPseudo">Pseudo</label>
                <input type="pseudo" class="form-control" id="exampleInputPseudo" aria-describedby="pseudoHelp" placeholder="Pseudo" name="pseudo" required>

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
            </div>

            <div class="row justify-content-center mt-4 mb-4">
                <button type="submit" class="btn btn-dark" name="button_connexion">Se connecter</button>
            </div>         
           
        </form>


    </div>

<?php
}
include 'footer.inc.php';
?>